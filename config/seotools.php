<?php
/**
 * @see https://github.com/artesaos/seotools
 */

return [
    'meta' => [
        /*
         * The default configurations to be used by the meta generator.
         */
        'defaults'       => [
            'title'        => false, // set false to total remove
            'titleBefore'  => false, // Put defaults.title before page title, like 'It's Over 9000! - Dashboard'
            'description'  => 'Hi! My name is Diki Akbar Asyidiq i\'m is a tech enthusiast, fullstack web developer, software engineer, i post daily about programming.', // set false to total remove
            'separator'    => ' - ',
            'keywords'     => ['Personal Blog', 'Web Developer', 'PHP', 'Fullstack Web Developer', 'Blog', 'Ruby'],
            'canonical'    => 'current', // Set to null or 'full' to use Url::full(), set to 'current' to use Url::current(), set false to total remove
            'robots'       => false, // Set to 'all', 'none' or any combination of index/noindex and follow/nofollow
            'images'      => [
                config('app.url').'/images/logo.jpeg'
            ],
        ],
        /*
         * Webmaster tags are always added.
         */
        'webmaster_tags' => [
            'google'    => null,
            'bing'      => null,
            'alexa'     => null,
            'pinterest' => null,
            'yandex'    => null,
            'norton'    => null,
        ],

        'add_notranslate_class' => false,
    ],
    'opengraph' => [
        /*
         * The default configurations to be used by the opengraph generator.
         */
        'defaults' => [
            'title'       => false, // set false to total remove
            'description' => 'Hi! My name is Diki Akbar Asyidiq i\'m is a tech enthusiast, fullstack web developer, software engineer, i post daily about programming.', // set false to total remove
            'url'         => 'current', // Set null for using Url::current(), set false to total remove
            'type'        => 'Blog',
            'site_name'   => 'codeWithDiki',
            'images'      => [
                config('app.url').'/images/logo.jpeg'
            ],
        ],
    ],
    'twitter' => [
        /*
         * The default values to be used by the twitter cards generator.
         */
        'defaults' => [
            'card'        => 'summary',
            'site'        => '@AsyidiqDiki',
        ],
    ],
    'json-ld' => [
        /*
         * The default configurations to be used by the json-ld generator.
         */
        'defaults' => [
            'title'       => false, // set false to total remove
            'description' => 'Hi! My name is Diki Akbar Asyidiq i\'m is a tech enthusiast, fullstack web developer, software engineer, i post daily about programming.', // set false to total remove
            'url'         => 'current', // Set to null or 'full' to use Url::full(), set to 'current' to use Url::current(), set false to total remove
            'type'        => 'WebPage',
            'images'      => [
                config('app.url').'/images/logo.jpeg'
            ],
        ],
    ],
];
