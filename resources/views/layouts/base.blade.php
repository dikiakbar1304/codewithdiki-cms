<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="bg-gray-200 scroll-smooth scrollbar-thin scrollbar-thumb-primary scrollbar-track-primary-300">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        @hasSection('title')

            <title>@yield('title') - {{ config('app.name') }}</title>
        @else
            <title>{{ config('app.name') }}</title>
        @endif

        {!! SEO::generate(true) !!}

        <!-- Favicon -->
		<link rel="shortcut icon" href="@yield('favicon', theme_asset('images/logo-colored.png'))">

        <!-- Fonts -->
        <link rel="stylesheet" href="https://rsms.me/inter/inter.css">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ theme_asset('css/app.css') }}">
        @livewireStyles
        <style>
            [x-cloak] { display: none !important; }
        </style>
        @toastScripts
        <!-- Scripts -->
        <script src="{{ theme_asset('js/app.js') }}" defer></script>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @stack('google-ads')

    </head>

    <body>
        <livewire:toasts />
        @yield('header')

        @yield('hero')

        <div class="w-full max-w-6xl mx-auto px-2 lg:px-4">
            @yield('body')

            <footer class="text-center font-semibold opacity-30 relative">
                Code With Diki - CMS {{ "@".now()->format('Y') }}
                <a href="https://codewithdiki.com" class="absolute inset-0"></a>
            </footer>
        </div>

        @livewireScripts
        

        @stack('scripts')

        @yield('google-analytics')
    </body>
</html>
