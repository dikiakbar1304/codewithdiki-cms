<div wire:poll="getLocations" class="space-y-1">
    @if($pages->count() > 0)
        @foreach($pages as $page)
            <div class="py-1 border-b flex justify-between items-center">
                <div>
                    {{ $page->country ?? "Unknown" }}
                </div>
                <div>
                    {{ $page->visitors }}
                </div>
            </div>
        @endforeach
    @endif
</div>
