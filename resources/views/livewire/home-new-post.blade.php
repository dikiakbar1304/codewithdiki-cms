<div class="w-full rounded-lg py-3 space-y-3">
    <h1 class="text-md text-lg font-bold text-primary-500 pb-2 border-b border-gray-300">
        Recent Posts
    </h1>
    @if($posts->count() > 0)
        <div class="grid grid-cols-1 lg:grid-cols-3 gap-3">
            @foreach($posts as $post)
                @include('components.post-card', ['post' => $post])
            @endforeach
        </div>
    @else
        @include('components.empty', ["message" => "Doesn't have post yet!"])
    @endif
    @if($show_next_button)
        <div class="flex justify-center items-center">
            <button class="py-1.5 px-3 text-white bg-primary-500 rounded-md flex justify-center gap-2 items-center" wire:loading.attr="disabled" wire:click="morePost()">
                <div wire:loading>
                    @svg('heroicon-o-refresh', 'w-5 h-5 animate-spin')
                </div>
                <div>
                    Load More
                </div>
            </button>
        </div>
    @endif
</div>
