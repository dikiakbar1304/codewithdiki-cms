<div x-data="{
    modal : false
}">
    <div class="flex justify-center items-center text-primary p-1 border border-primary rounded-lg cursor-pointer" @click="modal=true">
        @svg('heroicon-o-search', 'w-5 h-5')
    </div>

    <div class="fixed flex justify-center items-start inset-0 bg-gray-300 bg-opacity-10 backdrop-blur-md z-50 px-2" x-cloak x-show="modal">
        <div class="bg-white rounded-lg border border-primary px-3 py-4 w-full max-w-md mt-10 space-y-3" @click.away="modal=false">
            <div class="flex justify-end items-center">
                <button class="font-bold text-primary" @click="modal=false">
                    Close
                </button>
            </div>
            {{ $this->form }}

            @if($results != null)
                <div class="py-2 border-t">
                    <div class="text-center font-bold text-primary" wire:loading.block>
                        Loading ...
                    </div>
                    <div class="space-y-2 devide-y max-h-48 lg:max-h-min overflow-y-auto" wire:loading.remove>
                        @if($results->count() > 0)
                            @foreach($results as $result)
                                @include('components.search-result', ['post' => $result])
                            @endforeach
                        @else
                            @include('components.search-empty', ["message" => "Result of query '{$query}' not found .."])
                        @endif
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
