<div class="py-1 px-3 rounded-full flex items-center gap-1 border border-primary-500 text-primary-500 relative">
    @svg('heroicon-o-tag', 'w-5 h-5')
    <h3 class="text-xs font-semibold truncate">
        {{ $tag->name }}
    </h3>
    <a href="{{ route('tag.explore', ['tag_slug' => $tag->name]) }}" class="absolute inset-0"></a>
</div>