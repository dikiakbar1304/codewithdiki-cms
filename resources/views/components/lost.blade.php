<div class="flex justify-center items-center flex-col gap-6">
    <img src="{{ theme_asset('images/lost.svg') }}" alt="Empty Data" class="w-1/2">
    <div class="space-y-2">
        <div class="text-sm font-semibold">
            The page you are looking for is not available.
        </div>
        <a href="{{ route('home') }}" class="block py-3 px-4 text-center font-bold text-md text-white bg-primary-500 rounded-md">
            Home
        </a>
    </div>
</div>