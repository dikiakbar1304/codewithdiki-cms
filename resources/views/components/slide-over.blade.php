<div class="fixed lg:hidden inset-0 overflow-hidden z-50"
    aria-labelledby="slide-over-title"
    role="dialog"
    aria-modal="true"
    x-show="show_sidebar_container"
    x-cloak>
    <div class="absolute inset-0 overflow-hidden">
        <div class="absolute inset-0 bg-gray-500 bg-opacity-30" aria-hidden="true">
            <div class="fixed inset-y-0 right-0 pl-10 max-w-full flex">
                <div class="w-screen max-w-xs lg:max-w-md relative">
                    <div class="h-full flex flex-col py-6 bg-white shadow-xl overflow-y-scroll relative"
                    x-show="show_sidebar_content"
                    x-transition:enter="transition ease-out duration-300"
                    x-transition:enter-start="translate-x-64"
                    x-transition:enter-end="translate-x-0"
                    x-transition:leave="transition ease-in duration-300"
                    x-transition:leave-start="translate-x-0"
                    x-transition:leave-end="translate-x-64"
                    >
                        <div class="px-4 sm:px-6">
                            <div class="flex items-start justify-end">
                                <div class="ml-3 h-7 flex justify-between items-center w-full">
                                    @livewire('search-component')
                                    
                                    <button @click="show_sidebar_content = false;" @click.debounce.500ms="show_sidebar_container = false;" type="button" class="bg-white block rounded-md text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        <span class="sr-only">Close panel</span>
                                        <svg class="h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                                        </svg>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="mt-6 relative flex-1 px-4 sm:px-6 space-y-3">
                            <div class="flex gap-3 items-center py-2 border-t text-primary-500 text-lg font-bold relative">
                                <figure class="border rounded-lg p-2">
                                    @svg('heroicon-o-home', 'w-5 h-5')
                                </figure>
                                <h1 class="pt-1">
                                    Home
                                </h1>
                                <a href="{{ route('home') }}" class="absolute inset-0"></a>
                            </div>
                            <div class="max-h-64 overflow-y-auto space-y-3 ">
                                @foreach($categories as $category)
                                    <div class="flex gap-3 items-center py-2 border-t text-primary-500 text-lg font-bold relative">
                                        <figure class="border rounded-lg p-2">
                                            @svg('heroicon-o-folder', 'w-5 h-5')
                                        </figure>
                                        <h1 class="pt-1">
                                            {{ $category->name }}
                                        </h1>
                                        <a href="{{ route('category.explore', [
                                            'category_slug' => $category->slug,
                                            'utm_content' => 'mobile-sidebar-click'    
                                        ]) }}" class="absolute inset-0"></a>
                                    </div>
                                @endforeach
                            </div>

                            @if($tags)
                                <div class="px-2 py-3 bg-white border-t space-y-2" data-card-type="Post tag card">
                                    <h1 class="text-md">
                                        Tags
                                    </h1>
                                    <div class="flex gap-3 flex-wrap max-h-64 overflow-y-auto">
                                        @foreach($tags as $tag)
                                            @include('components.tag-item', ['tag' => $tag])
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>