<div class="flex relative gap-3">
    <div class="mr-4 flex-shrink-0 self-center">
        <img src="{{ $post->getThumbnailImageUrlAttribute() }}" alt="{{ $post->title }} thumbnail" class="w-16 h-16 object-cover border rounded-lg">
    </div>
    <div>
        <h4 class="font-bold text-primary">
            {{ $post->title }}
        </h4>
        <p class="mt-1 text-xs">
            {{ $post->excerpt }}
        </p>
    </div>
    <a href="{{ route('category.post.show', [
        'category_slug' => $post->category->slug,
        'post_slug' => $post->slug,
        'utm_content' => 'search_result_card',
        'utm_source' => 'SearchComponent'
    ]) }}" class="absolute inset-0"></a>
</div>