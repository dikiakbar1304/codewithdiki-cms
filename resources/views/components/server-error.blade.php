<div class="flex justify-center items-center flex-col gap-6">
    <img src="{{ theme_asset('images/server-error.svg') }}" alt="Empty Data" class="w-1/2">
    <div class="space-y-2">
        <div class="text-sm font-semibold">
            The server has failed, it will probably work again in a while.
        </div>
    </div>
</div>