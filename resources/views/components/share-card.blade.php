<div class="px-2 py-3 rounded-lg bg-white border space-y-2" data-card-type="share card" x-data="shareCardUtilities()">
    <h1 class="text-md">
        Share
    </h1>
    <div class="space-y-3">
        <div class="scrollbar-thin scrollbar-thumb-primary scrollbar-track-primary-300 w-full py-2 text-center px-4 flex text-sm border rounded-lg overflow-x-auto"
        @click="copyToClipboard('{{ get_current_url([
                    'utm_content' => 'share-link'    
                ]) }}')">
            <div class="">
                {{ get_current_url() }}
            </div>
            
        </div>
        <div class="flex justify-center items-center">
            <div class="text-xs font-bold text-primary-400 cursor-pointer">
                <div x-show="showFeedback" x-cloak>
                    Link Copied.
                </div>
            </div>
        </div>
    </div>
    <div class="flex gap-1 items-center justify-center py-2">
        <figure class="relative p-1 hover:bg-primary-500 hover:bg-opacity-20 rounded-full">
            <img src="{{ theme_asset('images/facebook-logo.png') }}" alt="Facebook share" class="h-8 w-auto">
            <a href="https://facebook.com/sharer/sharer.php?u={{ encoded_current_url([
                'utm_source' => 'Facebook',
                'utm_content' => 'facebook-share-button-click',    
            ]) }}" class="absolute inset-0" target="_BLANK"></a>
        </figure>
        <figure class="relative p-1 hover:bg-primary-500 hover:bg-opacity-20 rounded-full">
            <img src="{{ theme_asset('images/twitter-logo.png') }}" alt="Twitter Share" class="h-8 w-auto">
            <a href="https://twitter.com/intent/tweet/?url={{ encoded_current_url([
                'utm_source' => 'Twitter',
                'utm_content' => 'twitter-share-button-click',    
            ]) }}" class="absolute inset-0" target="_BLANK"></a>
        </figure>
        <figure class="relative p-1 hover:bg-primary-500 hover:bg-opacity-20 rounded-full">
            <img src="{{ theme_asset('images/reddit-logo.png') }}" alt="Reddit Share" class="h-8 w-auto">
            <a href="http://www.reddit.com/submit?url={{ encoded_current_url([
                'utm_source' => 'Reddit',
                'utm_content' => 'reddit-share-button-click',    
            ]) }}" class="absolute inset-0" target="_BLANK"></a>
        </figure>
    </div>
</div>

@push('scripts')
    <script>
        function shareCardUtilities(){
            return {
                showFeedback : false,
                copyToClipboard(url) {
                    let $tempInput = document.createElement('INPUT');

                    this.$el.appendChild($tempInput);
                    $tempInput.value = url;
                    $tempInput.select();
                    $tempInput.setSelectionRange(0, 99999);
                    document.execCommand('copy');
                    this.$el.removeChild($tempInput);;

                    this.showFeedback = true;

                    setTimeout(() => this.showFeedback = false, 3000);
                }
            };
        }
    </script>

@endpush