<div class="flex justify-center items-center flex-col gap-2">
    <img src="{{ theme_asset('images/empty.svg') }}" alt="Empty Data" class="w-1/2">
    <div class="text-danger-500 font-bold text-md">
        {{ $message ?? '' }}
    </div>
</div>