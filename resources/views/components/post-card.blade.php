<div class="w-full border h-full rounded-lg shadow-md bg-white transition ease-in-out delay-150 hover:-translate-y-0.5 hover:scale-105 duration-300">
    <div class="space-y-2">
        <figure class="h-48 relative">
            <img src="{{ $post->getThumbnailImageUrlAttribute() }}" alt="{{ $post->excerpt }}" class="h-full w-full object-cover rounded-t-lg">
            <a href="{{ route('category.post.show', [
                    'category_slug' => $post->category->slug,
                    'post_slug' => $post->slug,
                    'utm_content' => 'card:post-thumbnail-click'
                ]) }}" class="absolute inset-0">
            </a>
            <div class="absolute top-0 right-0 py-2 px-4 bg-zinc-300 bg-opacity-50 backdrop-blur-sm rounded-tr-lg font-bold rounded-bl-lg text-primary flex justify-center gap-0.5 text-md items-center">
                <div class="pt-1">
                    {{ $post->visitors()->notBot()->count() }}
                </div>
                @svg('heroicon-o-eye', 'w-5 h-5')
            </div>
        </figure>
        <div class="px-3 space-y-1 py-2">
            <h1 class="text-lg font-bold text-primary-500 relative">
                {{ $post->title }}
                <a href="{{ route('category.post.show', [
                        'category_slug' => $post->category->slug,
                        'post_slug' => $post->slug,
                        'utm_content' => 'card:post-name-click'
                    ]) }}" class="absolute inset-0">
                </a>
            </h1>
            <p class="text-xs font-semibold">
                {{ $post->excerpt }}
            </p>
        </div>
        @if($post->tags->count() > 0)
            <div class="flex max-w-content overflow-x-auto gap-2 px-3 pb-2 scrollbar-thin scrollbar-thumb-primary scrollbar-track-primary-300">
                @foreach($post->tags as $tag)
                    @include('components.tag-item', ['tag' => $tag])
                @endforeach
            </div>
        @endif
    </div>
</div>