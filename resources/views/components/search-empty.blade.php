<div class="flex justify-center items-center flex-col gap-2">
    <img src="{{ theme_asset('images/search-empty.svg') }}" alt="Empty Data" class="w-1/2">
    <div class="text-primary-500 font-bold text-md text-center">
        {{ $message ?? 'Empty result ...' }}
    </div>
</div>