@extends('layouts.app')

@section('title', $category->name)
@section('hero-banner', $banner)
@section('hero-banner-alt', $category->name)
@section('hero-header', $category->name)
@section('hero-content', $category->description)


@section('app-content')
    <div class="py-2 space-y-6">
        @livewire('category-new-post', [
            'category' => $category    
        ])
        <div class="w-full rounded-lg py-3 space-y-3">
            <h1 class="text-md text-lg font-bold text-primary-500 pb-2 border-b border-gray-300">
                Most Popular on {{ $category->name }}
            </h1>
            @if($mostPopularPosts->count() > 0)
                <div class="grid grid-cols-1 lg:grid-cols-3 gap-3">
                    @foreach($mostPopularPosts as $post)
                        @include('components.post-card', ['post' => $post])
                    @endforeach
                </div>
            @else
                @include('components.empty', ["message" => "No data found."])
            @endif
        </div>
    </div>
@endsection
