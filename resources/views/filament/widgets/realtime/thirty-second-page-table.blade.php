<x-filament::widget>
    <x-filament::card>
        <h1 class="text-lg font-bold">
            Page Visit
        </h1>
        @livewire('stats.realtime.thirty-second-page')
    </x-filament::card>
</x-filament::widget>
