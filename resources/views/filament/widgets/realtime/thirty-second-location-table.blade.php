<x-filament::widget>
    <x-filament::card>
        <h1 class="text-lg font-bold">
            Location
        </h1>
        @livewire('stats.realtime.thirty-second-location')
    </x-filament::card>
</x-filament::widget>