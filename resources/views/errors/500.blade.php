@extends('layouts.base')

@section('title', "500 Internal Server Error")

@section('body')

<div class="fixed inset-0 flex justify-center items-center w-full">
    @include('components.server-error')
</div>

@endsection