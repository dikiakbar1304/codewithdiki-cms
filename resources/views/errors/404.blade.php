@extends('layouts.base')

@section('title', "404 Not Found")

@section('body')

<div class="fixed inset-0 flex justify-center items-center w-full">
    @include('components.lost')
</div>

@endsection