<?php

use Spatie\LaravelSettings\Migrations\SettingsMigration;

class CreateTwitterSettings extends SettingsMigration
{
    public function up(): void
    {
        $this->migrator->add('twitter.bearer', '');
        $this->migrator->add('twitter.consumer_key', '');
        $this->migrator->add('twitter.consumer_secret', '');
        $this->migrator->add('twitter.access_token', '');
        $this->migrator->add('twitter.access_secret', '');
        $this->migrator->add('twitter.client_id', '');
        $this->migrator->add('twitter.client_secret', '');
        $this->migrator->add('twitter.refresh_token', '');
        $this->migrator->add('twitter.activate', false);
    }
}
