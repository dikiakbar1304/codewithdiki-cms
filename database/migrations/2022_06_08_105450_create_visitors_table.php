<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->id();
            
            $table->string('cookie')->nullable();
            $table->string('type')->nullable();
            $table->string('ip')->nullable();
            $table->text('agent')->nullable();
            $table->text('landing_params')->nullable();
            $table->string('referrer_domain')->nullable();
            $table->text('referrer_url')->nullable();
            $table->string('gclid')->nullable();
            $table->text('utm_source')->nullable();
            $table->text('utm_campaign')->nullable();
            $table->text('utm_medium')->nullable();
            $table->text('utm_term')->nullable();
            $table->text('utm_content')->nullable();
            $table->text('referral')->nullable();
            $table->integer('visitable_id')->nullable();
            $table->string('visitable_type')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
};
