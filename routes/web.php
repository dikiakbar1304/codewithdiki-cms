<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\AppController::class, 'landing'])->middleware([
    'statistic'
])->name('home');


Route::get('/tags/{tag_slug}', [\App\Http\Controllers\TagController::class, 'index'])->name('tag.explore');

Route::prefix('/{category_slug}')
->middleware([
    'statistic'
])
->name('category.')
->group(function(){
    Route::get('/', [\App\Http\Controllers\CategoryController::class, 'index'])->name('explore');


    Route::get('/{post_slug}', [\App\Http\Controllers\CategoryController::class, 'post_show'])->name('post.show');
});