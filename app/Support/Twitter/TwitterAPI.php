<?php

namespace App\Support\Twitter;

use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Subscriber\Oauth\Oauth1;

class TwitterAPI {
    
    protected ?string $bearer;
    protected ?string $consumer_key;
    protected ?string $consumer_secret;
    protected ?string $access_token;
    protected ?string $access_secret;
    protected ?string $client_id;
    protected ?string $client_key;
    protected ?string $refresh_token;
    protected ?bool $activate = false;

    final function __construct()
    {
        $this->bearer = app(\App\Settings\TwitterSettings::class)->bearer;
        $this->consumer_key = app(\App\Settings\TwitterSettings::class)->consumer_key;
        $this->consumer_secret = app(\App\Settings\TwitterSettings::class)->consumer_secret;
        $this->access_token = app(\App\Settings\TwitterSettings::class)->access_token;
        $this->access_secret = app(\App\Settings\TwitterSettings::class)->access_secret;
        $this->client_id = app(\App\Settings\TwitterSettings::class)->client_id;
        $this->client_key = app(\App\Settings\TwitterSettings::class)->bearer;
        $this->refresh_token = app(\App\Settings\TwitterSettings::class)->refresh_token;
        $this->activate = app(\App\Settings\TwitterSettings::class)->activate;
    }

    private function stack()
    {
        $stack = HandlerStack::create();

        $auth = new Oauth1([
            'consumer_key' => $this->consumer_key,
            'consumer_secret' => $this->consumer_secret,
            'token' => $this->access_token,
            'token_secret' => $this->access_secret,
        ]);

        $stack->push($auth);

        return $stack;
    }

    public function send_twit(\App\DTO\TwitterPostDTO $post) : void
    {

        $client = new Client([
            'base_uri' => 'https://api.twitter.com/1.1/',
            'handler' => $this->stack(),
            'auth' => 'oauth'
        ]);

        $query = Arr::query($post->toArray());

        if(!$this->activate){
            return;
        }

        $response = $client->post("statuses/update.json?{$query}");
    }


}