<?php

namespace App\Console\Commands;

use App\Models\Post;
use Illuminate\Console\Command;

class PublishPost extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publish:post';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Publish post scheduler';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $posts = Post::draft()
        ->where('published_at', '<', now())
        ->get();

        $posts->each(function(Post $post){
            $post->status = \App\Enum\PostStatusEnum::PUBLISHED()->value;
            $post->save();
            $published_at = $post->published_at->format('d/m/Y H:i:s');

            $category = $post->category;

            if(!$category->is_visible){
                $category->is_visible = true;

                $category->save();
            }

            $this->info("Post : {$post->name} was published at {$published_at}");
            \App\Jobs\SendTwitterJob::dispatch($post);
        });

        return 0;
    }
}
