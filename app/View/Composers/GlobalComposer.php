<?php

namespace App\View\Composers;

use Spatie\Tags\Tag;
use App\Models\Category;
use Illuminate\View\View;
use Illuminate\Support\Facades\Cache;

class GlobalComposer
{
    public function compose(View $view)
    {
        $view->with('categories', Cache::remember('global-view-categories', (60 * 60) * 24, function(){
            return Category::isVisible()->get();
        }));

        $view->with('tags', Cache::remember('global-view-tags', (60 * 60) * 24, function(){
            return Tag::all();
        }));
    }
}