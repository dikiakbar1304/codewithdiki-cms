<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendTwitterJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        public \App\Models\Post $post
    )
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $hashTags = $this->post->tags->map(function($tag){
            return "#".str_replace(" ", "", $tag->name);
        })->implode(' ');

        $url = route('category.post.show', [
            "category_slug" => $this->post->category->slug,
            "post_slug" => $this->post->slug
        ]);

        $status = "{$this->post->excerpt} \n {$hashTags} \n {$url}";

        $twitterPost = new \App\DTO\TwitterPostDTO([
            'status' => $status
        ]);

        (new \App\Support\Twitter\TwitterAPI())->send_twit($twitterPost);
        
    }
}
