<?php

namespace App\Jobs;

use App\Models\Visitor;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Contracts\Queue\ShouldQueue;

class TrackVisitJob implements ShouldQueue
{
    use Queueable;

    public function __construct(public \App\DTO\TrackVisitDTO $trackVisitDTO)
    {

    }

    public function handle()
    {
        

        $visit = Visitor::query()->create($this->trackVisitDTO->toArray());

        return;
    }
}
