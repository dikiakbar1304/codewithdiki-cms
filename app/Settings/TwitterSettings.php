<?php

namespace App\Settings;

class TwitterSettings extends \Spatie\LaravelSettings\Settings
{
    public string $bearer;
    public string $consumer_key;
    public string $consumer_secret;
    public string $access_token;
    public string $access_secret;
    public string $client_id;
    public string $client_secret;
    public string $refresh_token;
    public bool $activate;

    public static function group() : string
    {
        return "twitter";
    }
}