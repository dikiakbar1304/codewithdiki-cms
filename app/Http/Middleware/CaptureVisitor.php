<?php

namespace App\Http\Middleware;


use Closure;
use App\Models\Post;
use App\Models\Category;
use App\DTO\TrackVisitDTO;
use App\Jobs\TrackVisitJob;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\Resonse;
use Illuminate\Support\Facades\Route;

class CaptureVisitor
{
    /**
     * The Request instance.
     *
     * @var Request
     */
    protected $request;

    /**
     * The Request instance.
     *
     * @var Resonse
     */
    protected $response;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $type = null)
    {
        try {
            $this->request = $request;
    
            $this->response = $next($this->request);
    
            
    
            //Only track get requests
            if (!$this->request->isMethod('get')) {
                return $this->response;
            }
    
            $attributionData = $this->captureAttributionData();
            $cookieToken = $this->findOrCreateTrackingCookieToken();
    
    
            if (Route::is('category.explore')) {
                $category = Category::where('slug', $request->category_slug)->first();

                if($category){
                    $attributionData['visitable_id'] = $category->id;
                    $attributionData['visitable_type'] = Category::class;
                }
            }

            if(Route::is('category.post.show'))
            {
                $category = Category::where('slug', $request->category_slug)->first();

                if($category){
                    $post = $category->posts->where('slug', $request->post_slug)->first();


                    if($post){
                        $attributionData['visitable_id'] = $post->id;
                        $attributionData['visitable_type'] = Post::class;
                    }
                }
            }
            
            $attributionData['type'] = $type;
            $attributionData['ip'] = request()->ip();
            $attributionData['agent'] = request()->header('User-Agent');
    
            $trackVisitDTO = new TrackVisitDTO([
                'cookie' => $cookieToken,
                'type' => $attributionData['type'],
                'url' => $request->url(),
                'ip' => $attributionData['ip'],
                'agent' => $attributionData['agent'],
                'landing_params' => $attributionData['landing_params'],
                'referrer_domain' => $attributionData['referrer']['referrer_domain'],
                'referrer_url' => $attributionData['referrer']['referrer_url'],
                'gclid' => $attributionData['gclid'],
                'utm_source' => $attributionData['utm']['utm_source'],
                'utm_campaign' => $attributionData['utm']['utm_campaign'],
                'utm_medium' => $attributionData['utm']['utm_medium'],
                'utm_term' => $attributionData['utm']['utm_term'],
                'utm_content' => $attributionData['utm']['utm_content'],
                'referral' => $attributionData['referral'],
                'visitable_id' => $attributionData['visitable_id'] ?? null,
                'visitable_type' => $attributionData['visitable_type'] ?? null,
            ]);
            
    
            dispatch(new TrackVisitJob($trackVisitDTO));
            
            return $this->response;

        } catch (\Exception $e){
            \Illuminate\Support\Facades\Log::alert($e->getMessage());
        }
    }

    /**
     * @return array
     */
    protected function captureAttributionData()
    {
        $attributionData = [];

        

        $attributionData['landing_params'] = $this->captureLandingParams();
        $attributionData['referrer'] = $this->captureReferrer();
        $attributionData['gclid'] = $this->captureGCLID();
        $attributionData['utm'] = $this->captureUTM();
        $attributionData['referral'] = $this->captureReferral();

        return $attributionData;
    }

    /**
     * @return string
     */
    protected function captureLandingParams()
    {
        return $this->request->getQueryString();
    }

    /**
     * @return array
     */
    protected function captureUTM()
    {
        $parameters = ['utm_source', 'utm_campaign', 'utm_medium', 'utm_term', 'utm_content'];

        $utm = [];

        foreach ($parameters as $parameter) {
            if ($this->request->has($parameter)) {
                $utm[$parameter] = $this->request->input($parameter);
            } else {
                $utm[$parameter] = null;
            }
        }

        return $utm;
    }

    /**
     * @return array
     */
    protected function captureReferrer()
    {
        $referrer = [];

        $referrer['referrer_url'] = $this->request->headers->get('referer');

        $parsedUrl = parse_url($referrer['referrer_url']);

        $referrer['referrer_domain'] = isset($parsedUrl['host']) ? $parsedUrl['host'] : null;

        return $referrer;
    }

    /**
     * @return string
     */
    protected function captureGCLID()
    {
        return $this->request->input('gclid');
    }

    /**
     * @return string
     */
    protected function captureReferral()
    {
        return $this->request->input('ref');
    }

    /**
     * @return string $cookieToken
     */
    protected function findOrCreateTrackingCookieToken()
    {
        $cookieToken = Str::random(40);

        if ($this->request->hasCookie('statistic')) {
            $cookieToken = $this->request->cookie('statistic');
        }

        if (method_exists($this->response, 'withCookie')) {
            $this->response->withCookie(cookie('statistic', $cookieToken, strtotime('+1 days'), null, config('session.domain')));
        }

        return $cookieToken;
    }
}
