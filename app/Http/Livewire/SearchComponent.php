<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Illuminate\Support\Collection;
use Filament\Forms\Components\TextInput;

class SearchComponent extends Component implements \Filament\Forms\Contracts\HasForms
{
    use \Filament\Forms\Concerns\InteractsWithForms;
    
    public ?Collection $results = null;
    public ?string $query = null;

    public function mount() : void
    {
        $this->form->fill([]);
    }

    private function getQuery() : void
    {
        $query = Post::query()
        ->published()
        ->Where('title', 'like', "%{$this->query}%")
        ->orWhere('content', 'like', "%{$this->query}%")
        ->orWhere('excerpt', 'like', "%{$this->query}%")
        ->orWhereHas('category', function($query){
            $query->where('name', 'like', "%{$this->query}%");
        })
        ->orWhereHas('tags', function($query){
            $query->where('name', 'like', "%{$this->query}%");
        })
        ->orderBy('published_at', 'DESC')
        ->get();

        $this->results = $query->reject(function($post){
            return $post->status != \App\Enum\PostStatusEnum::PUBLISHED();
        })->take(3);
    }

    protected function updated($state) : void
    {
        if(strlen($this->query ?? '') < 1){
            $this->results = null;
            return;
        }

        $this->getQuery();
    }

    protected function getFormSchema(): array
    {
        return [
            TextInput::make('query')
            ->label("")
            ->helperText("Search post title, excerpt, and content ... ")
            ->reactive()
        ];
    }

    public function render()
    {
        return view('livewire.search-component');
    }
}
