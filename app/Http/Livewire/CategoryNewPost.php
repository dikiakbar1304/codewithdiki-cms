<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use App\Models\Category;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class CategoryNewPost extends Component
{
    public Category $category;
    public int $post_count = 0;
    public int $limit = 3;
    public ?Collection $posts = null;
    public bool $show_next_button = false;

    public function mount(Category $category)
    {
        $this->category = $category;
        $this->postCount();
        $this->getPosts();
    }

    private function postCount() : void
    {
        $this->post_count = $this->category->posts()->published()->count();
    }

    private function getPosts() : void
    {
        $this->posts = Cache::remember("category_posts_{$this->category->slug}", (60 * 60) * 24, function(){
            return Post::published()
            ->whereHas('category', function($query){
                $query->where('id', $this->category->id);
            })
            ->orderBy('published_at', 'DESC')
            ->get();
        })->take($this->limit);
        
        $this->showNextButton();
    }

    private function showNextButton() : void
    {
        $this->show_next_button = $this->limit < $this->post_count;
    }

    public function morePost() : void
    {
        $this->limit += 3;
        $this->getPosts();
    }

    public function render()
    {
        return view('livewire.category-new-post');
    }
}
