<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Livewire\Component;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class HomeNewPost extends Component
{
    public int $post_count = 0;
    public int $limit = 3;
    public ?Collection $posts = null;
    public bool $show_next_button = false;

    public function mount()
    {
        $this->postCount();
        $this->getPosts();
    }

    private function postCount() : void
    {
        $this->post_count = Post::published()->count();
    }

    private function getPosts() : void
    {
        $this->posts = Cache::remember('home_posts', (60 * 60) * 24, function(){
            return Post::published()
            ->orderBy('published_at', 'DESC')
            ->get();
        })
        ->take($this->limit);
        $this->showNextButton();
    }

    private function showNextButton() : void
    {
        $this->show_next_button = $this->limit < $this->post_count;
    }

    public function morePost() : void
    {
        $this->limit += 3;
        $this->getPosts();
    }

    public function render()
    {
        return view('livewire.home-new-post');
    }
}
