<?php

namespace App\Http\Livewire\Stats\Realtime;

use App\Models\Visitor;
use Livewire\Component;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Filament\Tables\Columns\TextColumn;

class ThirtySecondPage extends Component
{
    public $pages;

    public function mount() : void
    {
        $this->getPages();
    }
    
    public function getPages() : void
    {
        $this->pages = Visitor::
        select(DB::raw('distinct(count(cookie)) as visitors, url'))
        ->notBot()
        ->whereBetween('created_at', [
            now()->subMinutes(30),
            now()
        ])
        ->groupBy('url')
        ->orderBy('visitors', 'DESC')
        ->limit(6)
        ->get();

    }

    public function render()
    {
        return view('livewire.stats.realtime.thirty-second-page');
    }
}
