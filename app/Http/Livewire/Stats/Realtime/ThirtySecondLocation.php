<?php

namespace App\Http\Livewire\Stats\Realtime;

use App\Models\Visitor;
use Livewire\Component;
use Illuminate\Support\Facades\DB;
use Stevebauman\Location\Facades\Location;

class ThirtySecondLocation extends Component
{
    public $pages;

    public function mount() : void
    {
        $this->getLocations();
    }
    
    public function getLocations() : void
    {
        $this->pages = Visitor::
        select(DB::raw('distinct(count(cookie)) as visitors, ip'))
        ->notBot()
        ->whereBetween('created_at', [
            now()->subMinutes(30),
            now()
        ])
        ->groupBy('ip')
        ->orderBy('visitors', 'DESC')
        ->limit(6)
        ->get()->map(function($item){
            $location = Location::get($item->ip);

            $item->country = $location?->countryName ?? "(unknown)";

            return $item;
        });

    }

    public function render()
    {
        return view('livewire.stats.realtime.thirty-second-location');
    }
}
