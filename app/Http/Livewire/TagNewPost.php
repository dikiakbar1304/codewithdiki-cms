<?php

namespace App\Http\Livewire;

use App\Models\Post;
use Spatie\Tags\Tag;
use Livewire\Component;
use App\Models\Category;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class TagNewPost extends Component
{
    public Tag $tag;
    public int $post_count = 0;
    public int $limit = 3;
    public ?Collection $posts = null;
    public bool $show_next_button = false;

    public function mount(Tag $tag)
    {
        $this->tag = $tag;
        $this->postCount();
        $this->getPosts();
    }

    private function postCount() : void
    {
        $this->post_count = Post::withAnyTags([$this->tag->name])->published()->count();
    }

    private function getPosts() : void
    {
        $this->posts = Cache::remember("tag_posts_{$this->tag->id}", (60 * 60) * 24, function(){
            return Post::withAnyTags([$this->tag->name])
            ->published()
            ->orderBy('published_at', 'DESC')
            ->get();
        })
        ->take($this->limit);

        $this->showNextButton();
    }

    private function showNextButton() : void
    {
        $this->show_next_button = $this->limit < $this->post_count;
    }

    public function morePost() : void
    {
        $this->limit += 3;
        $this->getPosts();
    }

    public function render()
    {
        return view('livewire.tag-new-post');
    }
}
