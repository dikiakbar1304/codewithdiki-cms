<?php

namespace App\Http\Livewire;

use Spatie\Tags\Tag;
use Livewire\Component;
use App\Models\Subscriber;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\MultiSelect;
use Filament\Forms\Components\BelongsToManyMultiSelect;
use DanHarrin\LivewireRateLimiting\Exceptions\TooManyRequestsException;

class SubscribeEmail extends Component implements \Filament\Forms\Contracts\HasForms
{
    use \Filament\Forms\Concerns\InteractsWithForms, \DanHarrin\LivewireRateLimiting\WithRateLimiting, \Usernotnull\Toast\Concerns\WireToast;


    protected function getFormModel(): \Illuminate\Database\Eloquent\Model|string|null
    {
        return \App\Models\Subscriber::class;
    }

    public function mount() : void
    {

        $this->form->fill([]);
    }

    protected function getFormSchema(): array
    {
        return [
            Grid::make([
                'default' => 2
            ])->schema([
                TextInput::make('first_name')
                ->maxLength(25)
                ->required(),
                TextInput::make('last_name')
                ->maxLength(25)
                ->hint('(optional)'),
                TextInput::make('full_name')
                ->maxLength(50)
                ->required(),
                TextInput::make('email')
                ->email()
                ->required(),
            ]),
            BelongsToManyMultiSelect::make('categories')
            ->relationship('categories', 'name')
            ->helperText('Select categories you want to subscribe, leave blank if you want to subscribe to all categories.')
        ];
    }

    public function submit() : void
    {
        try {
            $this->rateLimit(1, 60);

            $this->subscribe();
        } catch (TooManyRequestsException $exception) {

            toast()->danger("Slow down! Please wait another $exception->secondsUntilAvailable seconds to subscribe.")->push();
            return;
        }
    }

    private function subscribe() : void
    {
        $this->form->validate();
        try{
            $data = $this->form->getState();
            $model = $this->getFormModel();

            $subscriber = $model::updateOrCreate([
                'email' => $data['email']
            ], $data);
 
            $this->form->model($subscriber)->saveRelationships();
            toast()->success('Thanks for subscribe to my personal blog 🤩')->push();
        } catch (\Exception $e){
            toast()->danger($e->getMessage())->push();
        }
    }

    public function render()
    {
        return view('livewire.subscribe-email');
    }
}
