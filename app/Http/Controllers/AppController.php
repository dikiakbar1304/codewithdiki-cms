<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class AppController extends Controller
{
    use SEOToolsTrait;
    
    public function landing(Request $request)
    {
        return view('home', (new \App\ViewModels\LandingViewModel));
    }
}
