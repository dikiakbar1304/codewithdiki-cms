<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\ViewModels\TagViewModel;

class TagController extends Controller
{
    public function index(Request $request, string $tag_slug) : View
    {
        return view('tag-explore', (new TagViewModel($tag_slug)));
    }
}
