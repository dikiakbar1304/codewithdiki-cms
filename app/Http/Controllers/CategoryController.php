<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use Illuminate\Http\Request;
use App\ViewModels\CategoryViewModel;
use Artesaos\SEOTools\Traits\SEOTools as SEOToolsTrait;

class CategoryController extends Controller
{
    use SEOToolsTrait;

    public function index(Request $request, string $category_slug) : View
    {
        $view_data = (new CategoryViewModel($category_slug));

        $this->seo()->setDescription($view_data->category->description);
        $this->seo()->opengraph()->setUrl($request->url());
        $this->seo()->opengraph()->addProperty('type', 'articles');
        $this->seo()->addImages([
            $view_data->category->getBannerImageUrlAttribute()
        ]);

        return view('category-explore', $view_data);
    }

    public function post_show(Request $request, string $category_slug, string $post_slug) : View
    {
        $view_data = (new CategoryViewModel($category_slug, $post_slug));

        $this->seo()->setDescription($view_data->post->excerpt);
        $this->seo()->opengraph()->setUrl($request->url());
        $this->seo()->jsonLd()->setType('Article');
        $this->seo()->metatags()->setKeywords($view_data->post->tags->pluck('name')->implode(', '));
        $this->seo()->addImages([
            $view_data->post->getBannerImageUrlAttribute()
        ]);

        return view('post-show', $view_data);
    }

}
