<?php

namespace App\Filament\Resources;

use Filament\Forms;
use Filament\Tables;
use App\Models\Category;
use Illuminate\Support\Str;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Components\Placeholder;
use Filament\Tables\Columns\BooleanColumn;
use App\Filament\Resources\CategoryResource\Pages;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use App\Filament\Resources\CategoryResource\RelationManagers;

class CategoryResource extends Resource
{
    protected static ?string $model = Category::class;

    protected static ?string $navigationIcon = 'heroicon-o-tag';
    protected static ?string $navigationGroup = 'Blog';

    protected static ?int $navigationSort = 1;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make([
                    'default' => 1,
                    'lg' => 3
                ])->schema([
                    Grid::make([
                        'default' => 1
                    ])->schema([
                        Card::make()
                        ->schema([
                           SpatieMediaLibraryFileUpload::make('banner')
                           ->collection('banner')
                           ->image()
                           ->imageCropAspectRatio('16:9')
                           ->imageResizeTargetWidth('1920')
                           ->imageResizeTargetHeight('1080'),
                           Grid::make([
                            'default' => 1,
                            'lg' => 2
                            ])->schema([
                                TextInput::make('name')
                                ->reactive()
                                ->required()
                                ->afterStateUpdated(fn(callable $set, ?string $state)
                                => $set('slug',Str::slug($state))),
                                TextInput::make('slug')
                                ->required()
                            ]),
                            Textarea::make('description')
                            ->rows(2)
                            ->required(),
                            Checkbox::make('is_visible')
                            ->label('Visible ? ')
                        ])
                    ])->columnSpan(2),
                    Grid::make([
                        'default' => 1
                    ])->schema([
                        Card::make()
                        ->schema([
                            Placeholder::make('created_at')
                            ->content(fn(?Category $record) 
                            => empty($record) ? "-" :
                            $record->created_at->format('d/m/Y H:i:s')),
                            Placeholder::make('updated_at')
                            ->content(fn(?Category $record) 
                            => empty($record) ? "-" :
                            $record->updated_at->format('d/m/Y H:i:s'))
                        ])
                    ])->columnSpan(1)
                ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('name')
                ->searchable(),
                TextColumn::make('slug')
                ->searchable(),
                BooleanColumn::make('is_visible'),
                TextColumn::make('created_at')
                ->dateTime('d/m/Y H:i'),
                TextColumn::make('updated_at')
                ->dateTime('d/m/Y H:i'),
            ])
            ->filters([
                //
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListCategories::route('/'),
            'create' => Pages\CreateCategory::route('/create'),
            'edit' => Pages\EditCategory::route('/{record}/edit'),
        ];
    }
}
