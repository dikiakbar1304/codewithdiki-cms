<?php

namespace App\Filament\Resources\PostResource\Pages;

use App\Filament\Resources\PostResource;
use Filament\Resources\Pages\ListRecords;

class ListPosts extends ListRecords
{
    protected static string $resource = PostResource::class;


    protected function getTableQuery(): \Illuminate\Database\Eloquent\Builder
    {
        $model = static::getModel();

        return $model::query()->orderBy('id', 'DESC');
    }

}
