<?php

namespace App\Filament\Resources;

use Filament\Forms;
use App\Models\Post;
use Filament\Tables;
use Illuminate\Support\Str;
use App\Enum\PostStatusEnum;
use Filament\Resources\Form;
use Filament\Resources\Table;
use Filament\Resources\Resource;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Select;
use Filament\Forms\Components\Textarea;
use Filament\Tables\Columns\TextColumn;
use Filament\Forms\Components\TextInput;
use Filament\Tables\Columns\BadgeColumn;
use Filament\Forms\Components\RichEditor;
use Filament\Tables\Actions\ButtonAction;
use Filament\Forms\Components\Placeholder;
use Filament\Forms\Components\DateTimePicker;
use App\Filament\Resources\PostResource\Pages;
use Filament\Forms\Components\BelongsToSelect;
use Filament\Forms\Components\SpatieTagsInput;
use App\Filament\Resources\PostResource\RelationManagers;
use Filament\Tables\Columns\SpatieMediaLibraryImageColumn;
use Filament\Forms\Components\SpatieMediaLibraryFileUpload;
use Mohamedsabil83\FilamentFormsTinyeditor\Components\TinyEditor;

class PostResource extends Resource
{
    protected static ?string $model = Post::class;

    protected static ?string $navigationIcon = 'heroicon-o-pencil-alt';
    protected static ?string $navigationGroup = 'Blog';

    protected static ?int $navigationSort = 2;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Grid::make([
                    'default' => 1,
                    'lg' => 3
                ])->schema([
                    Grid::make([
                        'default' => 1
                    ])->schema([
                        Card::make()
                        ->schema([
                            SpatieMediaLibraryFileUpload::make('Banner')
                            ->image()
                            ->imageCropAspectRatio('16:9')
                            ->imageResizeTargetWidth('1920')
                            ->imageResizeTargetHeight('1080')
                            ->collection('banner'),
                            Grid::make([
                                'default' => 1,
                                'lg' => 2
                            ])->schema([
                                TextInput::make('title')
                                ->reactive()
                                ->required()
                                ->afterStateUpdated(fn(callable $set, ?string $state)
                                => $set('slug',Str::slug($state))),
                                TextInput::make('slug')
                                ->required()
                            ]),
                            BelongsToSelect::make('category_id')
                            ->required()
                            ->relationship('category', 'name'),
                            Textarea::make('excerpt')
                            ->rows(2)
                            ->required(),
                            TinyEditor::make('content')
                            ->required(),
                            DateTimePicker::make('published_at')
                            ->label('Publish Schedule'),
                            Grid::make([
                                'default' => 1,
                                'lg' => 2
                            ])->schema([
                                Select::make('status')
                                ->required()
                                ->options(
                                    collect(PostStatusEnum::toValues())
                                    ->combine(PostStatusEnum::toLabels())
                                    ->map(function($item){
                                        return ucwords(str_replace('_', ' ', strtolower($item)));
                                    })
                                ),
                                SpatieTagsInput::make('tags'),
                            ])
                        ])
                    ])->columnSpan(2),
                    Grid::make([
                        'default' => 1
                    ])->schema([
                        Card::make()
                        ->schema([
                            Placeholder::make('created_at')
                            ->content(fn(?Post $record) 
                            => empty($record) ? "-" :
                            $record->created_at->format('d/m/Y H:i:s')),
                            Placeholder::make('updated_at')
                            ->content(fn(?Post $record) 
                            => empty($record) ? "-" :
                            $record->updated_at->format('d/m/Y H:i:s'))
                        ])
                    ])->columnSpan(1)
                ])
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                SpatieMediaLibraryImageColumn::make('banner')
                ->rounded()
                ->collection('banner'),
                TextColumn::make('title')
                ->sortable()
                ->searchable(),
                TextColumn::make('slug')
                ->sortable()
                ->searchable(),
                TextColumn::make('category.name')
                ->searchable(),
                BadgeColumn::make('status')
                ->formatStateUsing(fn(?string $state) => ucwords($state))
                ->colors([
                    'success' => PostStatusEnum::PUBLISHED()->value,
                    'warning' => PostStatusEnum::DRAFT()->value,
                    'danger' => PostStatusEnum::ARCHIVED()->value
                ]),
                TextColumn::make('visit_count')
                ->label('Seen')

            ])
            ->actions([
                ButtonAction::make('sendTwit')
                ->icon('fab-twitter')
                ->action(fn(Post $record) => \App\Jobs\SendTwitterJob::dispatch($record))
            ])
            ->filters([
                //
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPosts::route('/'),
            'create' => Pages\CreatePost::route('/create'),
            'edit' => Pages\EditPost::route('/{record}/edit'),
        ];
    }
}
