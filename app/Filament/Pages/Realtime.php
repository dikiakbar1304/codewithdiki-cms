<?php

namespace App\Filament\Pages;

use Filament\Pages\Page;

class Realtime extends Page
{
    protected static ?string $navigationIcon = 'heroicon-o-document-report';

    protected static ?string $navigationGroup = 'Analytics';

    protected static string $view = 'filament.pages.retention';

    protected function getHeaderWidgets(): array
    {
        return [
            \App\Filament\Widgets\Realtime\ThirtySecondStatsOverview::class,
            \App\Filament\Widgets\Realtime\ThirtySecondPageTable::class,
            \App\Filament\Widgets\Realtime\ThirtySecondLocationTable::class
        ];
    }

}
