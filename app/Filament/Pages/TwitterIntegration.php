<?php

namespace App\Filament\Pages;

use Filament\Pages\SettingsPage;
use App\Settings\TwitterSettings;
use Filament\Forms\Components\Card;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\Checkbox;
use Filament\Forms\Components\Textarea;
use Filament\Forms\Components\TextInput;
use Phpsa\FilamentPasswordReveal\Password;

class TwitterIntegration extends SettingsPage
{
    protected static ?string $navigationIcon = 'heroicon-o-cog';

    protected static string $settings = TwitterSettings::class;

    protected function getFormSchema(): array
    {
        return [
            Card::make()
            ->schema([
                Grid::make([
                    'default' => 2
                ])->schema([
                    TextInput::make('consumer_key')
                    ->required(),
                    Password::make('consumer_secret')
                    ->required(),
                    TextInput::make('access_token')
                    ->required(),
                    Password::make('access_secret')
                    ->required(),
                    TextInput::make('client_id'),
                    Password::make('client_secret')
                ]),
                Textarea::make('bearer'),
                Checkbox::make('activate')->default(false)
            ])
        ];
    }
}
