<?php

namespace App\Filament\Widgets;
use App\Models\Post;
use App\Models\Visitor;
use App\Models\Category;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;

class DailyVisitorChart extends \Filament\Widgets\LineChartWidget
{
    protected static ?string $heading = 'Daily Visitor Trend';
    protected static ?int $sort = 2;
    protected static ?string $pollingInterval = null;

    protected function getData(): array
    {
        $data = Trend::query(
            Visitor::notBot()
        )
        ->between(
            start: now()->startOfWeek(),
            end: now()->endOfWeek(),
        )
        ->perDay()
        ->count();

        $post_data = Trend::query(
            Visitor::query()
            ->notBot()
            ->where('visitable_type', Post::class)
        )
        ->between(
            start: now()->startOfWeek(),
            end: now()->endOfWeek(),
        )
        ->perDay()
        ->count();

        $category_data = Trend::query(
            Visitor::query()
            ->notBot()
            ->where('visitable_type', Category::class)
        )
        ->between(
            start: now()->startOfWeek(),
            end: now()->endOfWeek(),
        )
        ->perDay()
        ->count();

        return [
            'datasets' => [
                [
                    'label' => 'All Visit',
                    'data' => $data->map(fn (TrendValue $value) => $value->aggregate),
                    'borderColor' => '#5a49e3'
                ],
                [
                    'label' => 'Post Visit',
                    'data' => $post_data->map(fn (TrendValue $value) => $value->aggregate),
                    'borderColor' => '#22c55e'
                ],
                [
                    'label' => 'Category Visit',
                    'data' => $category_data->map(fn (TrendValue $value) => $value->aggregate),
                    'borderColor' => '#eab308'
                ],
            ],
            'labels' => $data->map(fn (TrendValue $value) => $value->date),
        ];
    }
}
