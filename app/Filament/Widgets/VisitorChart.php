<?php

namespace App\Filament\Widgets;

use App\Models\Post;
use App\Models\Visitor;
use App\Models\Category;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;
use Illuminate\Support\Facades\DB;
use Filament\Widgets\LineChartWidget;

class VisitorChart extends LineChartWidget
{
    protected static ?string $heading = 'Visitor Trend';
    protected static ?int $sort = 3;
    protected static ?string $pollingInterval = null;

    protected function getData(): array
    {
        $data = Trend::query(Visitor::notBot())
        ->between(
            start: now()->startOfWeek(),
            end: now()->endOfWeek(),
        )
        ->perDay()
        ->count(DB::raw('distinct(cookie)'));

        return [
            'datasets' => [
                [
                    'label' => 'Visitor',
                    'data' => $data->map(fn (TrendValue $value) => $value->aggregate),
                    'borderColor' => '#5a49e3'
                ]
            ],
            'labels' => $data->map(fn (TrendValue $value) => $value->date),
        ];
    }
}
