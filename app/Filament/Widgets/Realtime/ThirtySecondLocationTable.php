<?php

namespace App\Filament\Widgets\Realtime;

use Filament\Widgets\Widget;

class ThirtySecondLocationTable extends Widget
{
    protected static string $view = 'filament.widgets.realtime.thirty-second-location-table';
}
