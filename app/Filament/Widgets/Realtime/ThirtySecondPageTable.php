<?php

namespace App\Filament\Widgets\Realtime;

use Filament\Widgets\Widget;

class ThirtySecondPageTable extends Widget
{
    protected static string $view = 'filament.widgets.realtime.thirty-second-page-table';
}
