<?php

namespace App\Filament\Widgets\Realtime;

use App\Models\Visitor;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;
use Illuminate\Support\Facades\DB;
use Filament\Widgets\StatsOverviewWidget\Card;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;

class ThirtySecondStatsOverview extends BaseWidget
{
    public function mount()
    {

    }

    protected function getCards(): array
    {
        $visitors = Visitor::notBot()
        ->whereBetween('created_at', [
            now()->subMinutes(30),
            now()
        ])
        ->distinct()
        ->count('cookie');

        $visits = Visitor::notBot()
        ->whereBetween('created_at', [
            now()->subMinutes(30),
            now()
        ])
        ->count();

        return [
            Card::make('Visit Past 30 Minutes', $visits)
            ->color('success'),
            Card::make('Visitor Past 30 Minutes', $visitors)
            ->color('success'),
        ];
    }
}
