<?php

namespace App\Filament\Widgets;

use App\Models\Post;
use App\Models\Visitor;
use Flowframe\Trend\Trend;
use Flowframe\Trend\TrendValue;
use Illuminate\Support\Facades\DB;
use Filament\Widgets\StatsOverviewWidget\Card;
use Filament\Widgets\StatsOverviewWidget as BaseWidget;

class StatsOverview extends BaseWidget
{
    protected static ?int $sort = 1;
    protected static ?string $pollingInterval = null;

    protected function getCards(): array
    {
        $post_count = Post::all()->count();
        
        $past_month_visits = Visitor::notBot()
        ->whereMonth('created_at', now()->subMonth()->format('m'))
        ->whereYear('created_at', now()->subMonth()->format('Y'))
        ->count();

        $this_month_visits = Visitor::notBot()
        ->whereMonth('created_at', now()->format("m"))
        ->whereYear('created_at', now()->subMonth()->format('Y'))
        ->count();

        $visits_difference = $this_month_visits - $past_month_visits;

        $visits_color = ($visits_difference >= 0) ? 'success':'danger';

        $visitors = Visitor::query()
        ->notBot()
        ->whereBetween('created_at', [
            now()->startOfMonth(),
            now()->endOfMonth()
        ])
        ->distinct()
        ->count('cookie');

        $visitor_data = Trend::query(Visitor::notBot())
        ->between(
            start: now()->startOfMonth(),
            end: now()->endOfMonth(),
        )
        ->perDay()
        ->count(DB::raw('distinct(cookie)'));

        $past_month_visitors = Visitor::notBot()
        ->whereMonth('created_at', now()->subMonth()->format('m'))
        ->whereYear('created_at', now()->subMonth()->format('Y'))
        ->distinct('cookie')
        ->count();

        $this_month_visitors = Visitor::notBot()
        ->whereMonth('created_at', now()->format("m"))
        ->whereYear('created_at', now()->subMonth()->format('Y'))
        ->distinct('cookie')
        ->count();

        $visitors_difference = $this_month_visitors - $past_month_visitors;

        $visitors_color = ($visitors_difference >= 0) ? 'success':'danger';

        $visits = Visitor::query()
        ->notBot()
        ->whereBetween('created_at', [
            now()->startOfMonth(),
            now()->endOfMonth()
        ])
        ->count('cookie');

        $visit_data = Trend::query(Visitor::notBot())
        ->between(
            start: now()->startOfMonth(),
            end: now()->endOfMonth(),
        )
        ->perDay()
        ->count();

        return [
            Card::make('Posts', $post_count),
            Card::make('Visitors This Month', $visitors)
            ->chart($visitor_data->map(fn (TrendValue $value) => $value->aggregate)->toArray())
            ->description(($visitors_difference >= 0) ? "+{$visitors_difference} from last month":"{$visitors_difference} from last month")
            ->descriptionIcon(($visitors_difference >= 0) ? 'heroicon-s-trending-up':'heroicon-s-trending-down')
            ->color($visitors_color),
            Card::make('Visits This Month', $visits)
            ->chart($visit_data->map(fn (TrendValue $value) => $value->aggregate)->toArray())
            ->description(($visits_difference >= 0) ? "+{$visits_difference} from last month":"{$visits_difference} from last month")
            ->descriptionIcon(($visits_difference >= 0) ? 'heroicon-s-trending-up':'heroicon-s-trending-down')
            ->color($visits_color),
        ];
    }
}
