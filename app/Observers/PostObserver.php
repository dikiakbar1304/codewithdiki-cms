<?php

namespace App\Observers;

use App\Models\Post;
use App\Enum\PostStatusEnum;
use Illuminate\Support\Facades\Cache;

class PostObserver
{
    /**
     * Handle the Post "created" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */

    public function creating(Post $post)
    {
        $post->user_id = auth()->user()?->id;

        if($post->status == PostStatusEnum::PUBLISHED()->value){
            $post->published_at = now();
        }
    }

    public function created(Post $post)
    {
        forget_global_view_cache();
    }

    /**
     * Handle the Post "updated" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function updated(Post $post)
    {
        forget_global_view_cache();
    }

    /**
     * Handle the Post "deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function deleted(Post $post)
    {
        forget_global_view_cache();
    }

    /**
     * Handle the Post "restored" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function restored(Post $post)
    {
        forget_global_view_cache();
    }

    /**
     * Handle the Post "force deleted" event.
     *
     * @param  \App\Models\Post  $post
     * @return void
     */
    public function forceDeleted(Post $post)
    {
        forget_global_view_cache();
    }
}
