<?php

namespace App\Models;

use App\Enum\PostStatusEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Post extends Model implements \Spatie\MediaLibrary\HasMedia
{
    use HasFactory, 
    \Spatie\Tags\HasTags,
    \Illuminate\Database\Eloquent\SoftDeletes, 
    \Spatie\MediaLibrary\InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [
        'published_at' => 'datetime'
    ];

    protected $appends = [
        'visit_count'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')
        ->registerMediaConversions(function (Media $media) {
            $this
                ->addMediaConversion('banner-thumb')
                ->width(640)
                ->height(310);
        });
    }

    public function getBannerImageUrlAttribute()
    {
        $media = $this->getMedia('banner')->last();
        if ($media != null) {
            return $media->getUrl();
        }

        return config('app.placeholder_url');
    }

    public function getThumbnailImageUrlAttribute()
    {
        $media = $this->getMedia('banner')->last();
        if ($media != null) {
            return $media->getUrl('banner-thumb');
        }

        return config('app.placeholder_url');
    }

    public function creator() : BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function category() : BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    public function scopePublished($query)
    {
        return $query->where('status', PostStatusEnum::PUBLISHED()->value);
    }

    public function scopeDraft($query)
    {
        return $query->where('status', PostStatusEnum::DRAFT()->value);
    }

    public function visitors() : MorphMany
    {
        return $this->morphMany(Visitor::class, 'visitable');
    }

    public function getVisitCountAttribute()
    {
        return $this->visitors()->notBot()->count();
    }

}
