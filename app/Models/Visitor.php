<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $with = ['visitable'];

    public function visitable()
    {
        return $this->morphTo();
    }

    public function scopeNotBot($query)
    {
        return $query->where('agent', 'NOT LIKE', '%bot%')
        ->where('agent', 'NOT LIKE', '%Palo Alto%')
        ->where('agent', 'NOT LIKE', '%externalhit%')
        ->where('agent', 'NOT LIKE', '%WhatsApp%')
        ->where('agent', 'NOT LIKE', '%sindresorhus%')
        ->where('agent', 'NOT LIKE', '%Inspect%')
        ->where('agent', 'NOT LIKE', '%Ads%')
        ->where('agent', 'NOT LIKE', '%scrpit%')
        ->where('agent', 'NOT LIKE', '%service%')
        ->where('agent', 'NOT LIKE', null)
        ->where('agent', 'NOT LIKE', '%trend%')
        ->where('agent', 'NOT LIKE', '%twingly%')
        ->where('agent', 'NOT LIKE', '%crawler%')
        ->where('agent', 'NOT LIKE', '%curl%')
        ->where('agent', 'NOT LIKE', '%VLC%')
        ->where('agent', 'NOT LIKE', '%Astute%')
        ->where('agent', 'NOT LIKE', '%tweetedtimes%')
        ->where('agent', 'NOT LIKE', '%go-http-client%')
        ->where('agent', 'NOT LIKE', '%libranet.de%')
        ->where('agent', 'NOT LIKE', '%python%')
        ->where('agent', 'NOT LIKE', '%Media%')
        ->where('cookie', '!=', 'ZVgDJZAS2B3UGQ4yfOSBAEsi6lDiRJUccoeYaxCL');
    }

}
