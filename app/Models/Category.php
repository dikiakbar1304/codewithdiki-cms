<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Category extends Model implements \Spatie\MediaLibrary\HasMedia
{
    use HasFactory, \Spatie\MediaLibrary\InteractsWithMedia;

    protected $guarded = [];

    protected $casts = [
        'meta' => 'json'
    ];

    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('banner')
        ->registerMediaConversions(function (Media $media) {
            $this
                ->addMediaConversion('banner-thumb')
                ->width(640)
                ->height(310);
        });
    }

    public function getBannerImageUrlAttribute()
    {
        $media = $this->getMedia('banner')->last();
        if ($media != null) {
            return $media->getUrl();
        }

        return config('app.placeholder_url');
    }

    public function getThumbnailImageUrlAttribute()
    {
        $media = $this->getMedia('banner')->last();
        if ($media != null) {
            return $media->getUrl('banner-thumb');
        }

        return config('app.placeholder_url');
    }

    public function posts() : HasMany
    {
        return $this->hasMany(Post::class);
    }

    public function scopeIsVisible($query)
    {
        return $query->where('is_visible', true);
    }

    public function subscribers() : BelongsToMany
    {
        return $this->belongsToMany(Subscriber::class, 'category_subscriber');
    }

}
