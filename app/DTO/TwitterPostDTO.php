<?php

namespace App\DTO;

class TwitterPostDTO extends \Spatie\DataTransferObject\DataTransferObject
{
    public string $status;

    public ?int $in_reply_to_status_id;

    public ?string $attachment_url;
}