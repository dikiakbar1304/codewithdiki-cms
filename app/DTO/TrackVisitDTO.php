<?php

namespace App\DTO;

class TrackVisitDTO extends \Spatie\DataTransferObject\DataTransferObject
{
    public string $cookie;

    public ?string $type;

    public ?string $url;

    public ?string $ip;

    public ?string $agent;

    public ?string $landing_params;

    public ?string $referrer_domain;

    public ?string $referrer_url;

    public ?string $gclid;

    public ?string $utm_source;

    public ?string $utm_campaign;

    public ?string $utm_medium;

    public ?string $utm_term;

    public ?string $utm_content;

    public ?string $referral;

    public ?int $visitable_id;

    public ?string $visitable_type;

}