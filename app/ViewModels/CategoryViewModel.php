<?php

namespace App\ViewModels;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Collection;


class CategoryViewModel extends \Spatie\ViewModels\ViewModel
{
    public Category $category;
    public ?Post $post = null;

    public function __construct(string $slug, ?string $post_slug = null)
    {
        $category = Category::where('slug', $slug)->first();

        if(empty($category)){
            abort(404);
        }

        if($post_slug){
            $post = $category->posts()->where('slug', $post_slug)->first();

            if(empty($post)){
                abort(404);
            }

            $this->post = $post;
        }

        $this->category = $category;
    }

    public function mostPopularPosts() : Collection
    {
        return $this->category
        ->posts()
        ->published()
        ->withCount(['visitors as visit_time' => fn($query) => $query->notBot()])
        ->published()
        ->orderBy('visit_time', 'desc')->get();
    }
    
    public function banner() : ?string
    {
        return $this->category->getBannerImageUrlAttribute();
    }

    public function thumbnail() : ?string
    {
        return $this->category->getThumbnailImageUrlAttribute();
    }

    public function post_banner() : ?string
    {
        return $this->post?->getBannerImageUrlAttribute();
    }

    public function post_thumbnail() : ?string
    {
        return $this->post?->getThumbnailImageUrlAttribute();
    }

}