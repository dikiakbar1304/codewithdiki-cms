<?php

namespace App\ViewModels;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Collection;


class LandingViewModel extends \Spatie\ViewModels\ViewModel
{
    public function mostPopularArticles() : Collection
    {
        return Post::query()->withCount(['visitors as visit_time' => fn($query) => $query->notBot()])->published()->orderBy('visit_time', 'desc')->limit(6)->get();
    }

}