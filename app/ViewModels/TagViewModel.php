<?php

namespace App\ViewModels;

use App\Models\Post;
use Spatie\Tags\Tag;
use Illuminate\Support\Collection;


class TagViewModel extends \Spatie\ViewModels\ViewModel
{
    public Tag $tag;

    public function __construct(string $slug)
    {
        $tag = Tag::findFromStringOfAnyType($slug)->first();

        if(empty($tag)){
            abort(404);
        }

        $this->tag = $tag;
    }

    public function mostPopularPosts() : Collection
    {
        return Post::withAnyTags([$this->tag->name])
        ->published()
        ->withCount(['visitors as visit_time' => fn($query) => $query->notBot()])
        ->published()
        ->orderBy('visit_time', 'desc')
        ->get();
    }

}