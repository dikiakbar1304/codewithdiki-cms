<?php

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Artisan;

if(!function_exists('forget_global_view_cache'))
{
    function forget_global_view_cache() : void
    {
        Artisan::call('optimize');
        Artisan::call('optimize:clear');
    }
}


if(!function_exists('encoded_current_url')){
    function encoded_current_url(array $parameters = []) : string
    {
        if(empty($parameters)){
            return urlencode(url()->current());
        }

        return urlencode(request()->fullUrlWithQuery($parameters));
    }
}


if(!function_exists('get_current_url')){
    function get_current_url(array $parameters = []) : string
    {
        if(empty($parameters)){
            return url()->current();
        }

        return request()->fullUrlWithQuery($parameters);
    }
}